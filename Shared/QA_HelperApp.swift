//
//  QA_HelperApp.swift
//  Shared
//
//  Created by Branden Russell on 3/18/22.
//

import SwiftUI

@main
struct QA_HelperApp: App {
  var body: some Scene {
    WindowGroup {
      ContentView()
    }
  }
}
